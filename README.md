TDCR and Time Distribution Analysis for CAEN files
==================================================
Software for analyzing list-mode files from the CoMPASS software for CAEN
digitizers. The main purpose of the code is to get coincidence counting rates
(double and triple) and construct the time distributions for two or three
channels.

## About the project


## Installing
The code is written in the Rust programming language. In order to compile it you
must install [Rust](https://www.rust-lang.org/). Once installed, go to the
cdt_logc folder and run ```cargo build``` to compile the code. If there are no
errors you can install it on your system with ```cargo install --path=.```.

## Usage

After installation you can call the executable with ```cdt_logic -h```, to get a
list of the available command line arguments. The two necessary arguments are
the path to the folder with CAEN files. (ex. ./DAQ/Tritium_meas/UNFILTERED). and
the coincidence window in nanoseconds.


### Options

1. ```-n, --no-live```: Prints only the counting rates at the end of the
   measurement. This option disables the printing during the analysis of a
   measurement.
2. ```--ref-channel, --sec-channel```: Set the reference and secondary channels
   for the double coincidences timing histogram. For example, ```--ref-channel
       C --sec-channel A``` will output the histogram where the C channel gives
       the START time and the A channel gives the STOP time. Use this with the
       ```--ignore-third``` option if you do not want the third channel to incur
       dead-time on the system. 
       For more information on the different types of time distributions that can
       be constructed, please refer to the __Time distributions__ section.
3. ```-i, --ignore-third```: Ignores the third channel completely. The file of
   the third channel is not read and the third channel does not impose dead-time
   or affect the two other channels in any way. This option should be used when
   a simulation of a two-PMT from a three-PMT detector is required or when a two
   PMT detector is used.
4. ```-T, --triples```: Construct the time distribution of the triple
   coincidences.
5. ```-D, --doubles```: Construct the time distribution of the logical sum of
   double coincidences.
6. ```--triple-veto```: The time distribution of the pure double and pure logical
   sum of double coincidences will be constructed. Double events will be recorded 
   in the histogram only when we do not observe a triple coincidence.
7. ```-s, --show-hist```: Collect data about the time distribution between
   events and output a histogram at the end of the analysis.
8. ```-r, --hide-tdcr```: Do not output information about the counting rates,
   dead time or triple to double coincidence ratios.
9. ```-L, --hw```: Set the number of bins in the histogram. Note that each bin
   is 1 ns in size and covers the time at the center of the bin +/- 0.5 ns. For
   example, if we use ```-L 1000```, bin number 500 will be at a time difference
   0 ns and will cover the range between -0.5 ns and 0.5 ns.
10. ```-a, --asym-doubles```: Store the absolute time difference between the two
    channels in a double coincidences. With this option if B comes 5 ns after A
    or A comes 5 ns after B, both coincidences will be stored in the 5 ns time
    difference bin. Use this with the ```--ref-channel``` and
    ```--sec-channel``` options to get the coincidence counting rates for these
    channels at different coincidence windows.
11. ```-d, --dt```: Extending type dead-time duration in ns. Please note that
    the dead-time duration cannot be less than the coincidence window duration.
    The dead-time is started at the same time as the coincidence window and is
    extended if en event is detected during the dead-time, but after the end of
    the coincidence window. If using very long coincidence windows with length
    comparable to the dead-time length, this could violate the extended
    dead-time paradigm. The default value is 10 000 ns. So far the lenght or the
    type of dead-time (extending or not) does not seem to have a significant
    effect on the calculated efficiencies.

### Time distributions

#### Double coincidences time distributions from a two-PMT detector
In order to get the time distribution between two PMT of a two PMT detector or a
three PMT detector but ignoring the third channel you can use the following
setup:

```cdt_logic $PATH_TO_FILES $CW --ref-channel B --sec-channel C --ignore-third```

By ignoring the third channel completely it will not incur dead-time to the
system. You can combine this with the ```-a``` option to get the absolute value
of the time difference between the two channels. Please note that if the delay in
one of the channels is longer than the delay of the other channel the
distribution will not be symmetric around zero and this could lead to errors in
the distribution.


#### Double coincidences time distribution from a three-PMT detector
Similar to the two-PMT detector, for a three-PMT detector you need to specify
the reference and secondary channels with the following options:

```cdt_logic $PATH_TO_FILES $CW --ref-channel B --sec-channel C```

Here the third channel will contribute to the dead-time of the system. The time
difference that will be stored in this case is calculated by the
```update_double_histogram()``` function within [logic.rs](src/logic.rs).
This code will be executed as expected when having a pure double coincidence.
For example, let us suppose that we selected PMT B as the reference channel 
and PMT A as the secondary channel. If we have an AB (without ABC) coincidence 
within the selected coincidence window  the time difference that will be 
recorded in the histogram will be the time stamp of channel A - the time stamp 
of channel B. If we have a triple ABC coincidence durring the coincidence 
window the time difference between A and B will be recorded __only if__ the 
event in the third channel C arrives later than the event in A. If this is not
true, the triple coincidence will be recorded in the same way we record triple
coincidences. This behaviour is described in the software with the following 
piece of code part of the [``` update_histogram()``` function](src/logic.rs):

```rust
let rc = self.settings.reference_channel;
let sc = self.settings.secondary_channel;
let third = self.coinc_ts[ABC - rc - sc];
// Get the timestamp of the first arriving event
let first = f64::min(self.coinc_ts[rc], self.coinc_ts[sc]);
if third >= first || third == 0.0 {
    self.update_double_histogram();
} else {
    self.get_min_max_delta(ABC);
    self.update_t_or_d_histogram(ABC);
}
```
__Important:__ This is necessary because we want to obtain the number of counts
that will occur within a given coincidence window. If the third channel arrives 
within a coincidence window, but before the coincidence of the two other channels
it will trigger the dead-time of the detector. By excluding events for which the
third channel arrives before the reference and secondary we can obtain the
coincidence counting rate for a given coincidence window by integrating all
events from time difference zero to R, where R is the resolving time. In other
words: **This ensures that a recorded time difference will be detected as an AB 
coincidence by a coincidence window with the same width.**

### Logical sum of double coincidences time distribution
In order to construct the time distribution of the logical sum of double
coincidences we record the time between the __second arriving__ event in a
coincidence and the __first arriving__ event. This ensures that a recorded time
difference will be detected as a D coincidence by a coincidence window with the
same width.

### Triple coincidences time distribution
The triple coincidence time difference is calculated as the difference between
the __first arriving__ and the __last arriving__ event in a given window. This 
ensures that a recorded time difference will be detected as a T coincidence by 
a coincidence window with the same width.

### Output of the program
There are a few options to control the output of the program. When used without
options it prints the TDCR counting rates as they are being calculated. If you
want to store the output in a file use the ```-n``` option, which disables the
live printing of values and prints only the final result at the end of the
analysis. 

The TDCR output is structured as follows:

Dead time = 10000 ns	Coinc window = 40 ns

--------------------------------------------------------------------------------------------------------------------------------------------
| Time    | DT     | DT%  | A      | B      | C      | AB     | BC     | AC     | ABC    | D       | T/AB    | T/BC    | T/AC    | T/D     | 
|---------|--------|------|--------|--------|--------|--------|--------|--------|--------|---------|---------|---------|---------|---------|
| 4205.62 | 145.65 | 3.46 | 1759.7 | 1927.4 | 1668.6 | 808.57 | 800.92 | 810.71 | 539.00 | 1342.20 | 0.66661 | 0.67298 | 0.66485 | 0.40158 | 
--------------------------------------------------------------------------------------------------------------------------------------------

where 'Time' is the measurement time in seconds, 'DT' is the dead-time in
seconds, 'DT%' is the dead-time as a percentage of the total time, 'A..AB..ABC..'
are the counitng rages in counts per seconds and the rest are the TDCR values.

The output of a histogram is the following:

---------------------------------------------------------------------------------
| Time | PDF        | PDF unc     | Counts | CPS/ns | CPS total | CPS_corrected | 
|------|------------|-------------|--------|--------|-----------|---------------|
| -5   | 0.00000000 | 0.000000283 | 0      | 0.000  | 0.000     | 0.000         | 
| -4   | 0.04280848 | 0.000110088 | 151210 | 21.770 | 0.000     | 0.000         | 
| -3   | 0.05984071 | 0.000130159 | 211372 | 30.432 | 21.770    | 21.770        | 
| -2   | 0.08995075 | 0.000159579 | 317728 | 45.744 | 52.201    | 52.201        | 
| -1   | 0.12654279 | 0.000189275 | 446980 | 64.352 | 97.945    | 97.945        | 
| 0    | 0.15523248 | 0.000209636 | 548319 | 78.942 | 162.298   | 162.298       | 
| 1    | 0.17185223 | 0.000220573 | 607024 | 87.394 | 241.240   | 241.240       | 
| 2    | 0.15344806 | 0.000208428 | 542016 | 78.035 | 328.634   | 328.634       | 
| 3    | 0.11889467 | 0.000183466 | 419965 | 60.463 | 406.669   | 406.669       | 
| 4    | 0.08142982 | 0.000151833 | 287630 | 41.411 | 467.132   | 467.132       | 
| 5    | 0.00000000 | 0.000000283 | 0      | 0.000  | 508.543   | 508.543       | 
---------------------------------------------------------------------------------

Time is the time difference in nanoseconds, PDF is the normalized probability
for that time difference (counts / total counts), PDF unc is the uncertainty on
that value, CPS/ns is the counting rate in that channel (counts / live
time), CPS total is the integral from 0 to a given bin and CPS correted is
the CPS total - accidental coincidences counting rate for that resolving
time (bin).

## Contributing
If you spot a problem, don't hesitate to open an issue or send a mail to
ch.dutsov <at> phys dot uni dash sofia dot bg. I would be happy to assist you.
If you have a suggestion for expanding the code or changing it in some way, feel
free to open a pull request.

## License
This code is free software and is licensed under the GPL-v3 license. Feel free
to use, read, modify and distribute it as you please.
