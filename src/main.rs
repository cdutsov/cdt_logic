mod cdt_io;
mod config;
mod histograms;
mod logic;
mod static_const;
use cdt_io::{CsvRead, InputOutput, Readers};
use config::Config;
use histograms::Histograms;
use logic::{CurrentState, FinalState};

extern crate csv;

fn main() {
    // Read settings and print them for the user
    let settings = Config::new().unwrap();
    let mut final_histograms = Histograms::init_histograms(&settings);
    let mut final_state = FinalState::new();

    if !settings.hide_tdcr {
        settings.print_config();
        InputOutput::print_output_header(&settings);
    }

    if !settings.hide_config && settings.show_time_hist {
        settings.output_config();
    }

    for path in &settings.path_ls {
        let mut reader: Readers = match path {
            Some(p) => match InputOutput::new_reader(p.clone(), &settings) {
                Some(reader) => reader,
                None => {
                    panic!("Wrong folder selected! No .csv or .bin files here!");
                }
            },
            None => Readers::CsvRead(CsvRead::new()),
        };
        let mut histograms = Histograms::init_histograms(&settings);
        let mut state = CurrentState::new(&mut reader, &settings, &mut histograms);

        // Main loop of program
        let end_state = state.analyze_events();
        final_state.increment(
            end_state.dead_time.total_dt,
            end_state.total_coinc,
            end_state.current_evt.tm_stmp,
        );
        final_histograms.merge_histograms(state.histograms);
    }
    if settings.path_ls.len() > 1 && !settings.hide_tdcr {
        InputOutput::print_counting_rates(&final_state, &settings);
    }

    if settings.show_time_hist && !settings.timing_plot {
        final_histograms.output_time_hist(&final_state, &settings);
        final_state.print_footer(&settings);
    } else if settings.show_energy_hist {
        final_histograms.output_energy_hist(&settings);
        final_state.print_footer(&settings);
    } else if settings.show_pmt_energy {
        final_histograms.output_pmt_energy_hist(&settings);
        final_state.print_footer(&settings);
    } else if settings.timing_plot {
        final_histograms.output_timing_plot(&settings);
    }
}
