// Defintion of used constants. Please note that the values for each PMT
// are represented in binary form. To illustrate with an example: PMTA = 1;
// PMTC = 4; thus the AC coincidence has a value of AC = 1 + 4 = 5;

pub const D: usize = 0;
pub const A: usize = 1;
pub const B: usize = 2;
pub const C: usize = 4;
pub const AB: usize = 3;
pub const AC: usize = 5;
pub const BC: usize = 6;
pub const ABC: usize = 7;
pub const G: usize = 8;
pub const AG: usize = 9;
pub const BG: usize = 10;
pub const CG: usize = 12;
pub const ABG: usize = 11;
pub const BCG: usize = 14;
pub const ACG: usize = 13;
pub const TG: usize = 15;
pub const S: usize = 16;
pub const DG: usize = 17;
pub const SG: usize = 18;

// Event types; ev_type in struct Event
//
pub const NOISE: u32 = 0;
pub const PHOTON: u32 = 1;
pub const GAMMA: u32 = 2;
pub const EMPTY: u32 = 3;
pub const CW_END: u32 = 4;
pub const DT_END: u32 = 5;

pub const BUFFER_CAPACITY: usize = 6;
