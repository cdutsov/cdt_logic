use crate::logic::{Event, State};
use crate::static_const::*;
use crate::Config;
use std::collections::HashMap;
use std::convert::TryInto;
use std::fs;
use std::fs::File;
use std::io::Read;
use std::io::{self, BufRead, BufReader, Lines, Write};

pub struct InputOutput {}

impl InputOutput {
    pub fn new_reader(folder: String, settings: &Config) -> Option<Readers> {
        let mut new_reader: Option<Readers> = None;
        let mut file_readers: FileReaders = HashMap::with_capacity(4);
        let mut bin_readers: BinReaders = HashMap::with_capacity(4);

        for dir_entry in InputOutput::get_dir_entries(folder) {
            let pmt = FileRead::channel_from_name(dir_entry.path().to_str().unwrap());
            if (pmt == settings.third_channel as u32) && settings.ignore_third_channel {
                continue;
            };
            if !InputOutput::file_is_valid(&dir_entry, settings) {
                continue;
            }
            let path = dir_entry.path();
            let file: File = match File::open(&path) {
                Ok(f) => f,
                Err(e) => {
                    panic!("PROBLEM OPENING FILE: {}", e);
                }
            };
            if path.extension().unwrap().to_string_lossy() == "csv" {
                let (ts_pos_in_file, reader) = InputOutput::reader_from_csv(file);
                file_readers.insert(pmt, reader);
                if file_readers.len() == InputOutput::num_files_to_analyze(settings) {
                    new_reader = Some(Readers::FileRead(FileRead {
                        readers: file_readers,
                        current_line_number: [0; 4],
                        current_file_no: 0,
                        ts_pos_in_file,
                        timing_fit: settings.timing_fit,
                    }));
                    break;
                }
            } else if path.extension().unwrap().to_string_lossy() == "bin" {
                // reader.consume(buf_length);
                let (buf_length, reader) = match InputOutput::reader_from_bin(file) {
                    Some(r) => r,
                    None => continue,
                };
                bin_readers.insert(pmt, reader);
                if bin_readers.len() == InputOutput::num_files_to_analyze(settings) {
                    new_reader = Some(Readers::BinRead(BinRead {
                        readers: bin_readers,
                        buf_length,
                        timing_fit: settings.timing_fit,
                    }));
                    break;
                }
            }
        }
        match new_reader {
            Some(r) => Some(r),
            None => None,
        }
    }

    pub fn pmt_to_index(pmt: u32) -> usize {
        match pmt {
            1 => 0,
            2 => 1,
            4 => 2,
            8 => 3,
            _ => 2,
        }
    }

    pub fn index_to_pmt(index: usize) -> u32 {
        match index {
            0 => 1,
            1 => 2,
            2 => 4,
            3 => 8,
            _ => 4,
        }
    }

    pub fn bin_line_to_photon(line: &[u8]) -> (i64, u32, u32) {
        let ts = i64::from_le_bytes(line[4..12].try_into().expect("Error in binary timestamp"));
        let ln_gate =
            u16::from_le_bytes(line[12..14].try_into().expect("Error in long gate read")) as u32;
        let sh_gate =
            u16::from_le_bytes(line[14..16].try_into().expect("Error in short gate read")) as u32;
        (ts, sh_gate, ln_gate)
    }

    pub fn print_output_header(settings: &Config) {
        if settings.gamma_veto {
            println!(
                "{0: <10}{1: <9}{2: <9}{3: <9}{4: <9}{5: <9}{6: <9}{7: <9}\
             {8: <9}{9: <9}{10: <9}{11: <9}{12: <9}{13: <9}{14: <9}{15: <9}",
                "Time[s]",
                "DT[s]",
                "DT%",
                "AG",
                "BG",
                "CG",
                "G",
                "ABG",
                "BCG",
                "ACG",
                "TG",
                "DG",
                "TG/ABG",
                "TG/BCG",
                "TG/ACG",
                "TG/DG"
            );
        } else {
            println!(
                "{0: <10}{1: <9}{2: <9}{3: <9}{4: <9}{5: <9}{6: <9}{7: <9}\
             {8: <9}{9: <9}{10: <9}{11: <9}{12: <9}{13: <9}{14: <9}",
                "Time[s]",
                "DT[s]",
                "DT%",
                "A",
                "B",
                "C",
                "AB",
                "BC",
                "AC",
                "ABC",
                "D",
                "T/AB",
                "T/BC",
                "T/AC",
                "T/D"
            );
        }
    }

    pub fn print_counting_rates(s: &dyn State, settings: &Config) {
        let state = s.get_final_state();
        let elapsed_time = state.total_time;
        let dt = state.total_dt;
        let mut cnt_rates: [f64; 9] = [0.0; 9];
        let order_tdcr = [A, B, C, G, AB, BC, AC, ABC, D];
        let order = match settings.gamma_veto {
            true => [AG, BG, CG, G, ABG, BCG, ACG, TG, DG],
            false => order_tdcr,
        };
        for (i, ord) in order.iter().enumerate() {
            let res = state.total_coinc[*ord as usize] as f64 / (elapsed_time - dt) * 1e9;
            cnt_rates[order_tdcr[i]] = res;
        }
        let ratios = [
            cnt_rates[ABC] / cnt_rates[AB],
            cnt_rates[ABC] / cnt_rates[BC],
            cnt_rates[ABC] / cnt_rates[AC],
            cnt_rates[ABC] / cnt_rates[D],
        ];
        let sig_f = InputOutput::get_sig_f(cnt_rates[D]);
        let sig_f_et = 7 - f64::log10(elapsed_time / 1e9).ceil() as usize;
        // let sig_f_dt = 7 - f64::log10(dt / 1e9).ceil() as usize;
        print!("{:<10.*}", sig_f_et, elapsed_time / 1e9);
        print!("{:<9.*}", sig_f_et, dt / 1e9);
        print!("{:<9.*}", 2, dt / elapsed_time * 100.0);
        print!(
            "{:<9.*}",
            InputOutput::get_sig_f(cnt_rates[A]),
            cnt_rates[A]
        );
        print!(
            "{:<9.*}",
            InputOutput::get_sig_f(cnt_rates[B]),
            cnt_rates[B]
        );
        print!(
            "{:<9.*}",
            InputOutput::get_sig_f(cnt_rates[C]),
            cnt_rates[C]
        );
        if settings.gamma_veto {
            print!(
                "{:<9.*}",
                InputOutput::get_sig_f(cnt_rates[G]),
                cnt_rates[G]
            );
        }
        print!("{:<9.*}", sig_f, cnt_rates[AB]);
        print!("{:<9.*}", sig_f, cnt_rates[BC]);
        print!("{:<9.*}", sig_f, cnt_rates[AC]);
        print!("{:<9.*}", sig_f, cnt_rates[ABC]);
        print!("{:<9.*}", sig_f, cnt_rates[D]);
        print!("{:<9.*?}", 5, ratios[0]);
        print!("{:<9.*?}", 5, ratios[1]);
        print!("{:<9.*?}", 5, ratios[2]);
        print!("{:<9.*?}", 5, ratios[3]);
        io::stdout().flush().unwrap();
    }

    fn get_sig_f(cnt_rate: f64) -> usize {
        6 - f64::log10(cnt_rate).ceil() as usize
    }

    fn get_dir_entries(folder: String) -> Vec<std::fs::DirEntry> {
        let ls_dir = match fs::read_dir(folder) {
            Ok(f) => f,
            Err(e) => {
                panic!("CANNOT READ DIRECTORY: {}", e);
            }
        };
        let mut dir_entries: Vec<_> = ls_dir
            .map(|r| match r {
                Ok(r) => r,
                Err(e) => panic!("CANNOT READ DIRECTORY: {}", e),
            })
            .collect();
        dir_entries.sort_by_key(|dir| dir.path());
        dir_entries
    }

    fn num_files_to_analyze(settings: &Config) -> usize {
        let mut total_files = match settings.ignore_third_channel {
            true => 2,
            false => 3,
        };
        if settings.gamma_veto {
            total_files += 1;
        }
        if settings.gamma_only {
            total_files = 1;
        }
        total_files
    }

    fn file_is_valid(dir_entry: &std::fs::DirEntry, settings: &Config) -> bool {
        let path = dir_entry.path();
        let path_str = match path.to_str() {
            Some(p) => p,
            None => {
                panic!("Path not valid string!");
            }
        };
        if path.is_file()
            && path_str.contains("Data")
            && (path_str.contains("0@") && !settings.gamma_only
                || path_str.contains("1@") && !settings.gamma_only
                || path_str.contains("2@") && !settings.gamma_only
                || path_str.contains("3@"))
        {
            return true;
        }
        return false;
    }

    fn reader_from_csv(file: File) -> (usize, Lines<BufReader<File>>) {
        let mut reader: Lines<BufReader<File>> = BufReader::with_capacity(100000, file).lines();

        // Chech if file header has BOARD and CHANNEL info
        let mut ts_pos_in_file = 0;
        let header = match reader.next() {
            Some(h) => h.unwrap(),
            None => panic!("Error reading header of CSV file"),
        };
        if header
            .splitn(2, ';')
            .next()
            .unwrap_or("Header err")
            .contains("BOARD")
        {
            ts_pos_in_file = 2;
        }
        (ts_pos_in_file, reader)
    }

    fn reader_from_bin(file: File) -> Option<(usize, BufReader<File>)> {
        let mut reader: BufReader<std::fs::File> = BufReader::with_capacity(100_000_000, file);
        // Get buffer size
        let buffer = match reader.fill_buf() {
            Ok(b) => b,
            Err(e) => panic!("CANNOT FILL BUFFER{}", e),
        };
        if buffer.len() == 0 {
            return None;
        }

        let mut buf_length = i32::from_le_bytes(
            buffer[20..24]
                .try_into()
                .expect("Error getting data train size!"),
        ) as usize;
        if buf_length > 100000 {
            panic!("Data train size too big: {}\nProbably an error in the first lines of the binary file!\n", buf_length);
        }
        buf_length = buf_length * 2 + 24; // Add the waveform size to the event data which is 14 bytes
        Some((buf_length, reader))
    }
}

// File reading structs
type FileReaders = HashMap<u32, Lines<BufReader<File>>>;
type CsvReader = csv::Reader<io::Stdin>;
type BinReaders = HashMap<u32, BufReader<File>>;

pub enum Readers {
    CsvRead(CsvRead),
    FileRead(FileRead),
    BinRead(BinRead),
}

impl Readable for Readers {
    fn get_event(&mut self, pmt: u32) -> Option<Event> {
        match self {
            Readers::FileRead(reader) => reader.get_event(pmt),
            Readers::CsvRead(reader) => reader.get_event(pmt),
            Readers::BinRead(reader) => reader.get_event(pmt),
        }
    }
    fn len(&mut self) -> usize {
        match self {
            Readers::FileRead(reader) => reader.len(),
            Readers::CsvRead(reader) => reader.len(),
            Readers::BinRead(reader) => reader.len(),
        }
    }
}

pub struct CsvRead {
    reader: CsvReader,
}

impl CsvRead {
    pub fn new() -> CsvRead {
        CsvRead {
            reader: csv::Reader::from_reader(io::stdin()),
        }
    }
}

impl Readable for CsvRead {
    // Reader for Krasi's MC code
    fn get_event(&mut self, _pmt: u32) -> Option<Event> {
        let result = self.reader.records().next();
        let evt = match result {
            None => Event::empty(),
            Some(result) => {
                let record = result.expect("a CSV record");
                let mut pmt: u32 = record[4].parse::<u32>().expect("Error with PMT parsing");

                // Fix the numbering of PMTs in the raw file
                if pmt == 3 {
                    pmt = 4;
                }
                let ts_raw = record[1].parse::<f64>().expect("Error with ts");
                let tm_stmp = ts_raw * 1e9;
                let ev_raw = record[3].parse::<u32>().expect("Error with event type");
                let ev_type = match ev_raw {
                    1 => PHOTON,
                    2 => PHOTON,
                    3 => PHOTON,
                    0 => NOISE,
                    _ => NOISE,
                };
                let mut evt = Event {
                    tm_stmp,
                    sh_gate: 0,
                    ln_gate: 0,
                    ev_type,
                    pmt_num: pmt,
                    is_actv: true,
                    init_ev: false,
                };
                if ev_type == NOISE {
                    evt = self.get_event(pmt)?;
                }
                evt
            }
        };
        Some(evt)
    }

    fn len(&mut self) -> usize {
        3
    }
}

pub struct FileRead {
    readers: FileReaders,
    current_line_number: [u64; 4],
    current_file_no: usize,
    ts_pos_in_file: usize,
    timing_fit: [f64; 2],
}

impl FileRead {
    pub fn photon_from_file(&mut self, pos: u32) -> Option<(i64, u32, u32)> {
        let file_number = InputOutput::pmt_to_index(pos);
        self.current_file_no = file_number;
        self.current_line_number[file_number] += 1;
        if let Some(file_reader) = self.readers.get_mut(&pos) {
            let line = match file_reader.next() {
                None => None,
                Some(l) => {
                    let line = match l {
                        Ok(line) => line,
                        Err(e) => {
                            panic!(
                                "Error getting next line: {} from file {}\n{}",
                                self.current_line_number[file_number], file_number, e
                            );
                        }
                    };
                    Some(self.line_to_photon(line))
                }
            };
            line
        } else {
            None
        }
    }

    fn line_to_photon(self: &mut Self, line: String) -> (i64, u32, u32) {
        let mut items = line.splitn(4 + self.ts_pos_in_file, ';');
        let file_no = self.current_file_no as usize;
        let ln = self.current_line_number[file_no];
        for _ in 0..self.ts_pos_in_file {
            items.next();
        }
        let ts = match items.next() {
            Some(ts) => match ts.parse::<i64>() {
                Ok(s) => s,
                Err(e) => {
                    panic!(
                        "Error parsing time stamp on line {} in file {}\n{}",
                        ln, file_no, e
                    );
                }
            },
            None => -1,
        };
        let ln_gate = match items.next() {
            Some(ln_gate) => match ln_gate.parse::<u32>() {
                Ok(s) => s,
                Err(e) => {
                    panic!(
                        "Error parsing long gate on line {} in file {}\n{}",
                        ln, file_no, e
                    );
                }
            },
            None => 0,
        };
        let sh_gate = match items.next() {
            Some(sh_gate) => match sh_gate.parse::<u32>() {
                Ok(s) => s,
                Err(e) => {
                    panic!(
                        "Error parsing short gate on line {} in file {}\n{}",
                        ln, file_no, e
                    );
                }
            },
            None => 0,
        };
        (ts, sh_gate, ln_gate)
    }

    fn channel_from_name(path: &str) -> u32 {
        if path.contains("0@") {
            return 1;
        };
        if path.contains("1@") {
            return 2;
        };
        if path.contains("2@") {
            return 4;
        };
        if path.contains("3@") {
            return 8;
        };
        0
    }
}

pub struct BinRead {
    readers: BinReaders,
    buf_length: usize,
    timing_fit: [f64; 2],
}

impl BinRead {
    pub fn photon_from_file(&mut self, pos: u32) -> Option<(i64, u32, u32)> {
        let mut buf = vec![];
        if let Some(bin_reader) = self.readers.get_mut(&pos) {
            let line = match bin_reader
                .take(self.buf_length as u64)
                .read_to_end(&mut buf)
            {
                Err(e) => panic!("PROBLEM WITH BIN FILE {}", e),
                Ok(_) => {
                    if buf.len() < self.buf_length {
                        return None;
                    }
                    Some(InputOutput::bin_line_to_photon(&buf))
                }
            };
            line
        } else {
            None
        }
    }
}

pub trait Readable {
    fn get_event(&mut self, pmt: u32) -> Option<Event>;
    fn len(&mut self) -> usize;
}

impl Readable for FileRead {
    fn get_event(&mut self, pmt: u32) -> Option<Event> {
        let result = self.photon_from_file(pmt);
        let ev_type = match pmt {
            1 => PHOTON,
            2 => PHOTON,
            4 => PHOTON,
            8 => GAMMA,
            _ => EMPTY,
        };
        let evt = match result {
            Some((ts, sh_gate, ln_gate)) => Some(Event {
                tm_stmp: ts as f64 / 1e3 + linear_delay(ln_gate as f64, ev_type, self.timing_fit),
                sh_gate,
                ln_gate,
                ev_type,
                is_actv: true,
                pmt_num: pmt,
                init_ev: false,
            }),
            None => None,
        };
        evt
    }

    fn len(&mut self) -> usize {
        self.readers.len()
    }
}

impl Readable for BinRead {
    fn get_event(&mut self, pmt: u32) -> Option<Event> {
        let result = self.photon_from_file(pmt);
        let ev_type = match pmt {
            1 => PHOTON,
            2 => PHOTON,
            4 => PHOTON,
            8 => GAMMA,
            _ => EMPTY,
        };
        let evt = match result {
            Some((ts, sh_gate, ln_gate)) => Some(Event {
                tm_stmp: ts as f64 / 1e3 + linear_delay(ln_gate as f64, ev_type, self.timing_fit),
                sh_gate,
                ln_gate,
                ev_type,
                is_actv: true,
                pmt_num: pmt,
                init_ev: false,
            }),
            None => None,
        };
        evt
    }

    fn len(&mut self) -> usize {
        self.readers.len()
    }
}

// Used to fix the lack of CDF in the gamma channel.
// HACK
fn linear_delay(ln_gate: f64, ev_type: u32, timing_fit: [f64; 2]) -> f64 {
    if ev_type == GAMMA {
        let a = timing_fit[0];
        let b = timing_fit[1];
        -(a * ln_gate + b)
    } else {
        0.0
    }
}
