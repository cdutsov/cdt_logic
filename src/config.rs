use crate::static_const::*;
extern crate argparse;
use argparse::{ArgumentParser, List, Print, Store, StoreTrue};

//Holds the user input data.
pub struct Config {
    pub cw_duration: f64,
    pub ext_dt: f64,
    // pub path: Option<String>,
    pub path_ls: Vec<Option<String>>,
    pub hide_tdcr: bool,
    pub show_time_hist: bool,
    pub hist_trips: bool,
    pub hist_doubles: bool,
    pub hist_size: u64,
    pub energy_hist_size: u64,
    pub energy_bin_size: u32,
    pub out_hist_size: u64,
    pub bin_size: f64,
    pub asymmetrical_doubles: bool,
    pub mid: u64,
    pub triple_veto: bool,
    pub reference_channel: usize,
    pub secondary_channel: usize,
    pub third_channel: usize,
    pub ignore_third_channel: bool,
    pub reference_coinc: usize,
    pub disable_live_cps: bool,
    pub path_str: String,
    pub hide_config: bool,
    pub time_limit: f64,
    pub gamma_veto: bool,
    pub gamma_only: bool,
    pub show_energy_hist: bool,
    pub energy_ranges: [Vec<u32>; 4],
    pub show_pmt_energy: bool,
    pub gain_adjustment: Vec<f64>,
    pub correct_accidentals: bool,
    pub sorted_timestamps: bool,
    pub non_extended_deadtime: bool,
    pub timing_plot: bool,
    pub n_energy_bins: u32,
    pub timing_fit: [f64; 2],
}

impl Config {
    // Create a new instance of Config and read user
    // input as command line arguments
    pub fn new() -> Result<Config, &'static str> {
        let n_energy_bins = 16385 as u32;
        let mut path_str = "".to_string();
        let mut path_ls: Vec<Option<String>> = vec![];
        let mut paths: Vec<String> = vec![];
        let mut hide_config = false;
        let mut hist_trips = false;
        let mut show_time_hist = false;
        let mut hide_tdcr = false;
        let mut asymmetrical_doubles = false;
        let mut ext_dt = 10_000 as f64;
        let mut cw_duration = 200 as f64;
        let mut out_hist_size = 0 as u64;
        let mut triple_veto = false;
        let mut hist_doubles = false;
        let mut reference_channel_in = 'A';
        let mut secondary_channel_in = 'B';
        let mut ignore_third_channel = false;
        let mut disable_live_cps = false;
        let mut mid = 0;
        let mut bin_size = 1.0;
        let mut time_limit = 0.0;
        let mut gamma_veto = false;
        let mut gamma_only = false;
        let mut show_energy_hist = false;
        let mut energy_hist_size = n_energy_bins.into();
        let mut energy_bin_size = 1;
        let mut energy_range_a = vec![0, n_energy_bins];
        let mut energy_range_b = vec![0, n_energy_bins];
        let mut energy_range_c = vec![0, n_energy_bins];
        let mut energy_range_g = vec![0, n_energy_bins];
        let mut show_pmt_energy = false;
        let mut gain_adjustment = vec![1.0, 1.0, 1.0, 1.0];
        let mut correct_accidentals = false;
        let mut sorted_timestamps = false;
        let mut non_extended_deadtime = false;
        let mut timing_plot = false;
        // let mut timing_fit_str = String::from("-0.0569 571.9");
        let timing_fit_str_default = String::from("-0.0250678 -235.918");
        let mut timing_fit_str = String::from("");

        {
            let mut ap = ArgumentParser::new();
            ap.set_description(
                "Rust program that can be used to \
                 apply CDT TDCR logic to CAEN files",
            );
            ap.add_option(
                &["-V", "--version"],
                Print("cdt_logic version: ".to_owned() + &env!("CARGO_PKG_VERSION").to_string()),
                "Show version",
            );
            ap.refer(&mut path_str)
                .add_argument("path", Store, "Path to CAEN files folder");
            ap.refer(&mut cw_duration)
                .add_argument(
                    "cw",
                    Store,
                    "Coincidence window duration \
                              (default 200), ns",
                )
                .required();
            ap.refer(&mut ext_dt).add_option(
                &["-d", "--dt"],
                Store,
                "Dead-time duration (default 10 000), ns",
            );
            ap.refer(&mut energy_hist_size).add_option(
                &["--energy-hist-size"],
                Store,
                "Gamma Energy upper limit (default 2^14), LSB",
            );
            ap.refer(&mut energy_bin_size).add_option(
                &["--energy-bin-size"],
                Store,
                "Gamma energy bin size (default 1), LSB",
            );
            ap.refer(&mut out_hist_size).add_option(
                &["-L", "--hw"],
                Store,
                "Histogram size in channels (default: 1000)",
            );
            ap.refer(&mut bin_size).add_option(
                &["-b", "--bin-size"],
                Store,
                "Bin size in nanoseconds (default: 1 ns)",
            );
            ap.refer(&mut time_limit).add_option(
                &["-l", "--time-limit"],
                Store,
                "Time limit for measurement (default: 0 [whole file]), s",
            );
            ap.refer(&mut gamma_veto).add_option(
                &["-g", "--gamma-veto"],
                StoreTrue,
                "Gate on gamma channel (default: false)",
            );
            ap.refer(&mut gamma_only).add_option(
                &["--gamma-only"],
                StoreTrue,
                "Ignore TDCR channels and use only Gamma (default: false)",
            );
            ap.refer(&mut correct_accidentals).add_option(
                &["-c", "--correct-acc"],
                StoreTrue,
                "Correct for accidental coincidences (default: false)",
            );
            ap.refer(&mut show_time_hist).add_option(
                &["-s", "--show-time-hist"],
                StoreTrue,
                "Show time distribution histogram",
            );
            ap.refer(&mut show_pmt_energy).add_option(
                &["-P", "--show-pmt-energy"],
                StoreTrue,
                "Show the PMT energies histogram",
            );
            ap.refer(&mut show_energy_hist).add_option(
                &["-e", "--show-energy-hist"],
                StoreTrue,
                "Show gamma energy spectrum",
            );
            ap.refer(&mut timing_plot).add_option(
                &["-t", "--show-timing-plot"],
                StoreTrue,
                "Show timing between LS and gamma in 2D plot",
            );
            ap.refer(&mut timing_fit_str).add_option(
                &["--timing-fit-params"],
                Store,
                "Timing fit parameters in T(x) = a*x + b (default: \"-0.0250678 -235.918\")",
            );
            ap.refer(&mut hide_config).add_option(
                &["--hide-config"],
                StoreTrue,
                "Hide the output the configuration of the run",
            );
            ap.refer(&mut hide_tdcr).add_option(
                &["-r", "--hide-tdcr"],
                StoreTrue,
                "Hide TDCR output",
            );
            ap.refer(&mut hist_trips).add_option(
                &["-T", "--triples"],
                StoreTrue,
                "Show time dist histogram for T events",
            );
            ap.refer(&mut hist_doubles).add_option(
                &["-D", "--doubles"],
                StoreTrue,
                "Show time dist histogram for D events",
            );
            ap.refer(&mut ignore_third_channel).add_option(
                &["-i", "--ignore-third"],
                StoreTrue,
                "Ignore the third channel. Set reference and \
                secodary channels if using this option",
            );
            ap.refer(&mut asymmetrical_doubles).add_option(
                &["-a", "--asym-doubles"],
                StoreTrue,
                "One sided doubles histogram",
            );
            ap.refer(&mut triple_veto).add_option(
                &["--triple-veto"],
                StoreTrue,
                "Count only pure double coincidences for histogram",
            );
            ap.refer(&mut disable_live_cps).add_option(
                &["-n", "--no-live"],
                StoreTrue,
                "Disable live cps printing (prints only end result)",
            );
            ap.refer(&mut reference_channel_in).add_option(
                &["--ref-channel"],
                Store,
                "Reference channel for time difference histogram",
            );
            ap.refer(&mut secondary_channel_in).add_option(
                &["--sec-channel"],
                Store,
                "Secondary channel for time difference histogram",
            );
            ap.refer(&mut energy_range_a).add_option(
                &["--sca-A"],
                List,
                "PMT channel A energy range (default: 0 16385), channels",
            );
            ap.refer(&mut energy_range_b).add_option(
                &["--sca-B"],
                List,
                "PMT channel B energy range (default: 0 16385), channels",
            );
            ap.refer(&mut energy_range_c).add_option(
                &["--sca-C"],
                List,
                "PMT channel C energy range (default: 0 16385), channels",
            );
            ap.refer(&mut energy_range_g).add_option(
                &["--sca"],
                List,
                "Gamma energy range (default: 0 16385), channels",
            );
            ap.refer(&mut gain_adjustment).add_option(
                &["--gain"],
                List,
                "Gain multiplier per channel (default: 1.0 1.0 1.0 1.0)",
            );
            ap.refer(&mut paths).add_option(
                &["--paths"],
                List,
                "Paths to analyze. Should be last option",
            );
            ap.refer(&mut non_extended_deadtime).add_option(
                &["--non-ext-dt"],
                StoreTrue,
                "Use non-extending type dead-time. Use with caution!",
            );
            ap.silence_double_dash(false);
            ap.parse_args_or_exit();
        }
        if ext_dt < cw_duration {
            return Err("The dead-time base is shorter \
                       than the coincidence window!");
        }

        if !(path_str.contains("--")) {
            paths.reverse();
            let mut paths_r: Vec<Option<String>> = paths.iter().map(|p| Some(p.clone())).collect();
            let path = Some(path_str.clone());
            path_ls.append(&mut paths_r);
            path_ls.push(path);
            path_ls.reverse();
        } else {
            sorted_timestamps = true;
            path_ls.push(None);
        }

        if timing_plot {
            if cw_duration < 1000.0 {
                eprintln!("WARN: Coincidence window is probably too short for a meaningful plot. Try 2000 ns and more.");
            }
            if timing_fit_str.is_empty() {
                timing_fit_str = String::from("0.0 0.0");
            }
        } else if timing_fit_str.is_empty() {
            timing_fit_str = timing_fit_str_default;
        }

        let timing_fit;
        let timing_fit_vec: Vec<f64> = timing_fit_str
            .split_whitespace()
            .map(|s| s.parse().unwrap())
            .collect();
        if timing_fit_vec.len() != 2 {
            println!();
            panic!("Too many or too few timing fit parameters!")
        } else {
            timing_fit = [timing_fit_vec[0], timing_fit_vec[1]];
        }

        let hist_size;
        if asymmetrical_doubles || hist_trips || hist_doubles {
            hist_size = f64::ceil(cw_duration / bin_size) as u64;
        } else {
            hist_size = f64::ceil(cw_duration / bin_size) as u64 * 2;
            mid = hist_size / 2;
        }
        if out_hist_size == 0 {
            out_hist_size = hist_size;
        } else if asymmetrical_doubles || hist_trips || hist_doubles {
            out_hist_size *= f64::ceil(1.0 / bin_size) as u64;
        } else {
            out_hist_size *= f64::ceil(2.0 / bin_size) as u64;
        }

        if gamma_only {
            gamma_veto = true;
            show_energy_hist = true;
        }

        if show_energy_hist || show_time_hist {
            hide_tdcr = true;
        }

        if timing_plot {
            hide_tdcr = true;
            show_time_hist = true;
            secondary_channel_in = 'G';
            gamma_veto = true;
        }

        let reference_channel = Config::channel_in_to_usize(reference_channel_in);
        let secondary_channel = Config::channel_in_to_usize(secondary_channel_in);
        let third_channel = ABC - reference_channel - secondary_channel;
        let reference_coinc = match gamma_veto {
            false => reference_channel + secondary_channel,
            true => {
                if reference_channel == G || secondary_channel == G {
                    reference_channel + secondary_channel
                } else {
                    reference_channel + secondary_channel + G
                }
            }
        };
        let energy_ranges = [
            energy_range_a,
            energy_range_b,
            energy_range_c,
            energy_range_g,
        ];

        Ok(Config {
            cw_duration,
            ext_dt,
            // path,
            sorted_timestamps,
            path_ls,
            path_str,
            hide_tdcr,
            hist_trips,
            energy_hist_size,
            energy_bin_size,
            energy_ranges,
            show_time_hist,
            show_energy_hist,
            bin_size,
            hist_size,
            out_hist_size,
            asymmetrical_doubles,
            mid,
            triple_veto,
            hist_doubles,
            reference_channel,
            secondary_channel,
            third_channel,
            ignore_third_channel,
            reference_coinc,
            disable_live_cps,
            hide_config,
            time_limit,
            timing_fit,
            gamma_veto,
            gamma_only,
            show_pmt_energy,
            gain_adjustment,
            correct_accidentals,
            non_extended_deadtime,
            timing_plot,
            n_energy_bins,
        })
    }

    // Print the user input to standard output
    pub fn print_config(&self) {
        println!(
            "Dead time = {} ns\tCoinc window = {} ns",
            self.ext_dt, self.cw_duration
        );
    }

    fn channel_in_to_usize(chn: char) -> usize {
        match chn {
            'A' => 1,
            'B' => 2,
            'C' => 4,
            'G' => 8,
            _ => unreachable!(),
        }
    }

    fn usize_to_channel(chn: usize) -> char {
        match chn {
            1 => 'A',
            2 => 'B',
            4 => 'C',
            8 => 'G',
            _ => unreachable!(),
        }
    }

    pub fn output_config(&self) {
        let mut out_conf = String::new();
        let version = option_env!("PROJECT_VERSION").unwrap_or(env!("CARGO_PKG_VERSION"));
        out_conf += format!("# Configuration of the cdt_logic program\n").as_str();
        out_conf += format!("# Program version: {}\n", version).as_str();
        out_conf += format!("# Coincidence window: {}\n", self.cw_duration).as_str();
        out_conf += format!("# Dead time duration: {}\n", self.ext_dt).as_str();
        out_conf += format!("# Path to CAEN files: {}\n", self.path_str).as_str();
        out_conf += format!(
            "# Reference channel: {}\n",
            Config::usize_to_channel(self.reference_channel)
        )
        .as_str();
        out_conf += format!(
            "# Secondary channel: {}\n",
            Config::usize_to_channel(self.secondary_channel)
        )
        .as_str();
        out_conf += format!("# Ignore third channel: {}\n", self.ignore_third_channel).as_str();
        out_conf += format!("# Veto triple coincidences: {}\n", self.triple_veto).as_str();
        out_conf += format!("# Bin width in ns: {}\n", self.bin_size).as_str();
        out_conf += format!("# Time hist total bins: {}\n", self.out_hist_size).as_str();
        out_conf += format!("# Show T distribution: {}\n", self.hist_trips).as_str();
        out_conf += format!("# Show D distribution: {}\n", self.hist_doubles).as_str();
        out_conf += format!("# Asymetrical doubles: {}\n", self.asymmetrical_doubles).as_str();
        out_conf += format!("# Hide TDCR values: {}\n", self.hide_tdcr).as_str();
        out_conf += format!("# Output time hist: {}\n", self.show_time_hist).as_str();
        out_conf += format!("# Output energy hist: {}\n", self.show_energy_hist).as_str();
        out_conf += format!("# Gate on gamma channel: {}\n", self.gamma_veto).as_str();
        out_conf += format!("# Show only gamma channel: {}\n", self.gamma_only).as_str();
        out_conf += format!(
            "# Non-extending type dead-time: {}\n",
            self.non_extended_deadtime
        )
        .as_str();
        out_conf += format!(
            "# Lower/upper gates A: {} {}, B: {} {}, C: {} {}\n",
            self.energy_ranges[0][0],
            self.energy_ranges[0][1],
            self.energy_ranges[1][0],
            self.energy_ranges[1][1],
            self.energy_ranges[2][0],
            self.energy_ranges[2][1],
        )
        .as_str();
        out_conf += format!(
            "# Gamma energy lower/upper gate: {} {}\n",
            self.energy_ranges[3][0], self.energy_ranges[3][1]
        )
        .as_str();
        out_conf += format!(
            "# Timing dT(x) = {} * x + {}, where x is channel number\n",
            self.timing_fit[0], self.timing_fit[1]
        )
        .as_str();
        out_conf += format!("# =========================================").as_str();
        println!("{}", out_conf);
    }
}
