extern crate histogram;
use crate::config::Config;
use crate::logic::FinalState;
use crate::static_const::*;
use histogram::Histogram;
use std::ops::AddAssign;

#[derive(Debug, Copy, Clone)]
pub struct TdcrEnergy {
    pub energy: u64,
    pub total_coinc: [i32; 19],
}

impl TdcrEnergy {
    pub fn new() -> TdcrEnergy {
        TdcrEnergy {
            energy: 0,
            total_coinc: [0; 19],
        }
    }

    pub fn increment(&mut self, energy: u64, coinc: [bool; 19]) {
        self.energy = energy;
        for i in 0..coinc.len() {
            if coinc[i] {
                self.total_coinc[i] += 1;
            }
        }
    }
}

impl AddAssign for TdcrEnergy {
    fn add_assign(&mut self, other: Self) {
        for (aref, bref) in self.total_coinc.iter_mut().zip(other.total_coinc.iter()) {
            *aref += bref;
        }
    }
}

pub struct Histograms {
    pub energy: Histogram,
    pub tdcr_vs_energy: Vec<TdcrEnergy>,
    pub time_delta: Histogram,
    pub pmt_energy: [Histogram; 8],
    pub timing: Vec<Vec<i32>>,
}

impl Histograms {
    fn new_histogram(size: u64) -> Histogram {
        return match Histogram::configure()
            .max_value(size)
            .max_memory(2 * 1024 * 1024)
            .precision(8)
            .build()
        {
            Some(h) => h,
            None => Histogram::configure()
                .max_value(size)
                .max_memory(2 * 1024 * 1024)
                .precision(4)
                .build()
                .unwrap(),
        };
    }

    pub fn init_histograms(settings: &Config) -> Histograms {
        let e_hist_size =
            (settings.energy_hist_size as f64 / settings.energy_bin_size as f64) as u64;

        let energy = Histograms::new_histogram(e_hist_size);
        let time_delta = Histograms::new_histogram(settings.hist_size);
        let mut tdcr_vs_energy: Vec<TdcrEnergy> = vec![TdcrEnergy::new()];
        let pmt_energy = [
            Histograms::new_histogram(e_hist_size), // A
            Histograms::new_histogram(e_hist_size), // B
            Histograms::new_histogram(e_hist_size), // AB
            Histograms::new_histogram(e_hist_size), // C
            Histograms::new_histogram(e_hist_size), // AC
            Histograms::new_histogram(e_hist_size), // BC
            Histograms::new_histogram(e_hist_size), // ABC
            Histograms::new_histogram(e_hist_size), // D
        ];

        for _ in 0..e_hist_size {
            tdcr_vs_energy.push(TdcrEnergy::new());
        }

        let timing = vec![vec![0; settings.hist_size as usize]; 500];
        Histograms {
            pmt_energy,
            energy,
            time_delta,
            tdcr_vs_energy,
            timing,
        }
    }

    pub fn merge_histograms(&mut self, other_histograms: &Histograms) {
        self.time_delta.merge(&other_histograms.time_delta);
        for i in 0..self.tdcr_vs_energy.len() {
            self.tdcr_vs_energy[i] += other_histograms.tdcr_vs_energy[i];
        }
        for i in 0..self.pmt_energy.len() {
            self.pmt_energy
                .get_mut(i)
                .unwrap()
                .merge(&other_histograms.pmt_energy[i]);
        }
        self.energy.merge(&other_histograms.energy);
        for x in 0..self.timing.len() {
            for y in 0..self.timing[x].len() {
                self.timing[x][y] = other_histograms.timing[x][y];
            }
        }
    }

    pub fn output_time_hist(&self, state: &FinalState, settings: &Config) {
        let histogram = &self.time_delta;
        let mid = settings.mid;
        let buckets = histogram.buckets_total();
        let bin_size = settings.bin_size;
        let cutoff = (2000.0 / bin_size) as u64;
        // Start and end of output histogram
        let start = if mid == 0 {
            0
        } else {
            mid - settings.out_hist_size / 2
        };
        let end = if mid == 0 {
            settings.out_hist_size
        } else {
            settings.out_hist_size / 2 + mid + 1
        };

        // For randoms linear fit
        let mut x: Vec<f64> = vec![];
        let mut y: Vec<f64> = vec![];

        let lt = (state.total_time - state.total_dt) / 1e9;

        // The total counts in the range that we are outputting
        let mut total_counts = 0.0;
        // let mut randoms_in_plateau = 0.0;
        for i in 0..buckets {
            let counts = histogram.get(i).unwrap() as f64;
            if i >= start && i <= end {
                total_counts += counts;
            }
            if i > mid + cutoff && settings.correct_accidentals {
                x.push((i as f64 - mid as f64) * bin_size);
                y.push(counts / bin_size);
            }
        }

        // TODO: move to separate module
        let (a, b) = match settings.cw_duration < 3000.0 {
            true => Histograms::lin_fit(x, y),
            false => Histograms::log_fit(x, y),
        };
        let randoms_dist = match settings.cw_duration < 3000.0 {
            true => Histograms::lin_dist,
            false => Histograms::exp_dist,
        };
        let randoms_cdf = match settings.cw_duration < 3000.0 {
            true => Histograms::lin_cdf,
            false => Histograms::exp_cdf,
        };
        println!(
            "{:10} {:^12} {:^12} {:^10} {:^10} {:^10} {:^10}",
            "# Time", "PDF_corr", "Stdev", "RealCnts", "CDF_norm", "CDF_cps", "CDF_cps_corr",
        );

        let total_randoms = randoms_cdf(a, b, (end - start) as f64 * bin_size);
        let mut real_counts_0 = 0.0;
        let mut bin_corr;
        for i in start..end {
            let time_delta = (i as f64 - mid as f64) * bin_size;
            let counts = histogram.get(i).unwrap() as f64;
            if (settings.asymmetrical_doubles || settings.hist_trips || settings.hist_doubles)
                && time_delta == 0.0
            {
                bin_corr = 2.0;
                real_counts_0 = counts - randoms_dist(a, b, time_delta) * bin_size * bin_corr;
            } else {
                bin_corr = 1.0;
            }
            let randoms_per_channel = randoms_dist(a, b, time_delta) * bin_size * bin_corr;
            let real_counts = counts - randoms_per_channel;
            let norm_bin_corrected =
                real_counts / (total_counts - total_randoms + real_counts_0) / bin_size * bin_corr;

            // TODO: CDF of two sided distribution is not this
            let cdf_norm: f64 = (((mid as u64)..i)
                .map(|j: u64| (histogram.get(j).unwrap() as f64))
                .sum::<f64>()
                - randoms_cdf(a, b, time_delta))
                / (total_counts - total_randoms);
            let cdf: f64 = ((mid as u64)..i)
                .map(|j: u64| histogram.get(j).unwrap() as f64 / lt)
                .sum::<f64>();
            let cdf_corrected = cdf - randoms_cdf(a, b, time_delta) / lt;
            let mut stdev = 1.0 / total_counts / bin_size;
            if real_counts > 0.0 {
                stdev = (real_counts).sqrt() / total_counts / bin_size;
            }
            println!(
                "{:<10.4} {:12.8} {:12.9} {:10.0} {:10.5} {:10.2} {:10.2}",
                time_delta, norm_bin_corrected, stdev, real_counts, cdf_norm, cdf, cdf_corrected,
            );
        }
    }

    fn lin_fit(x: Vec<f64>, y: Vec<f64>) -> (f64, f64) {
        if x.len() > 2 && y.len() > 2 {
            let mean_x: f64 = x.iter().sum::<f64>() / x.len() as f64;
            let mean_y: f64 = y.iter().sum::<f64>() / y.len() as f64;
            let stdev_x: f64 = x.iter().map(|x| f64::powf(x - mean_x, 2.0)).sum();
            let cov_xy: f64 = x
                .iter()
                .zip(y.iter())
                .map(|(x, y)| (x - mean_x) * (y - mean_y))
                .sum::<f64>();
            let b = cov_xy / stdev_x;
            let a = mean_y - b * mean_x;
            (a, b)
        } else {
            (0.0, 0.0)
        }
    }

    fn log_fit(x: Vec<f64>, y: Vec<f64>) -> (f64, f64) {
        if x.len() > 2 && y.len() > 2 {
            let log_y: Vec<f64> = y
                .iter()
                .map(|y| match *y > 0.0 {
                    true => f64::ln(*y),
                    false => 0.0,
                })
                .collect();
            let mean_x: f64 = x.iter().sum::<f64>() / x.len() as f64;
            let mean_y: f64 = log_y.iter().sum::<f64>() / log_y.len() as f64;
            let stdev_x: f64 = x.iter().map(|x| f64::powf(x - mean_x, 2.0)).sum();
            let cov_xy: f64 = x
                .iter()
                .zip(log_y.iter())
                .map(|(x, y)| (x - mean_x) * (y - mean_y))
                .sum::<f64>();
            let b = cov_xy / stdev_x;
            let a = mean_y - b * mean_x;
            (f64::exp(a), b)
        } else {
            (0.0, -1.0)
        }
    }

    fn lin_dist(a: f64, b: f64, x: f64) -> f64 {
        a + b * f64::abs(x)
    }

    fn lin_cdf(a: f64, b: f64, x: f64) -> f64 {
        a * x + b * f64::powf(x, 2.0) / 2.0
    }

    fn exp_dist(a: f64, b: f64, x: f64) -> f64 {
        a * f64::exp(b * f64::abs(x))
    }

    fn exp_cdf(a: f64, b: f64, x: f64) -> f64 {
        a * (f64::exp(x * b) - 1.0) / b
    }

    pub fn output_energy_hist(&self, settings: &Config) {
        settings.output_config();
        println!(
            "{:10} {:^12} {:^12} {:^12} {:^12} {:^12} {:^12}",
            "# Channel", "TotCounts", "AB", "BC", "AC", "D", "T"
        );

        for bin in 0..self.energy.buckets_total() as usize {
            let energy = bin * settings.energy_bin_size as usize;
            println!(
                "{:<10} {:10} {:10} {:10} {:10} {:10} {:10}",
                energy,
                self.energy.get(bin as u64).unwrap(),
                self.tdcr_vs_energy[bin].total_coinc[ABG],
                self.tdcr_vs_energy[bin].total_coinc[BCG],
                self.tdcr_vs_energy[bin].total_coinc[ACG],
                self.tdcr_vs_energy[bin].total_coinc[DG],
                self.tdcr_vs_energy[bin].total_coinc[TG],
            );
        }
    }

    pub fn output_pmt_energy_hist(&self, settings: &Config) {
        settings.output_config();
        println!(
            "{:10} {:^12} {:^12} {:^12} {:^12} {:^12} {:^12} {:^12} {:^12}",
            "# Channel", "A", "B", "C", "AB", "BC", "AC", "D", "T"
        );
        for bin in 0..self.energy.buckets_total() as usize {
            let energy = bin * settings.energy_bin_size as usize;
            println!(
                "{:<10} {:10} {:10} {:10} {:10} {:10} {:10} {:10} {:10}",
                energy,
                self.pmt_energy[A].get(bin as u64).unwrap(),
                self.pmt_energy[B].get(bin as u64).unwrap(),
                self.pmt_energy[C].get(bin as u64).unwrap(),
                self.pmt_energy[AB].get(bin as u64).unwrap(),
                self.pmt_energy[BC].get(bin as u64).unwrap(),
                self.pmt_energy[AC].get(bin as u64).unwrap(),
                self.pmt_energy[D].get(bin as u64).unwrap(),
                self.pmt_energy[ABC].get(bin as u64).unwrap(),
            );
        }
    }

    pub fn output_timing_plot(&self, settings: &Config) {
        println!("{:10} {:^12}", "# Channel", "TimeDiff");
        for gate_bin in 0..self.timing.len() {
            let mut gate =
                (gate_bin as f64 + 0.5) / self.timing.len() as f64 * settings.n_energy_bins as f64;
            gate = gate.floor();
            let max = self.timing[gate_bin].iter().max().unwrap();
            let max_pos = self.timing[gate_bin]
                .iter()
                .position(|el| el == max)
                .unwrap();
            let td = max_pos as f64 / self.timing[gate_bin].len() as f64
                * settings.hist_size as f64
                - settings.mid as f64;
            if max_pos != 0 {
                println!("{:<10} {:<10}", gate, td as i32);
            } else {
                println!("{:<10} {:<10}", gate, '-');
            }
        }
    }
}
