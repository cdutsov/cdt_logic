use crate::cdt_io::{InputOutput, Readable, Readers};
use crate::config::Config;
use crate::histograms::Histograms;
use crate::static_const::*;
use std::cmp::Ordering;

//Definition of used structs. Event holds all the necessarry event info.
//tm_stmp: is the timestamp of the event
//ev_type: holds the type of event (decay or noise event)
//pmt_num: the number of the PMT that was hit (A = 1; B = 2; C = 4)
//is_activ: if is_active is __false__ the counting stops
#[derive(Debug)]
pub struct Event {
    pub tm_stmp: f64,
    pub sh_gate: u32,
    pub ln_gate: u32,
    pub ev_type: u32,
    pub pmt_num: u32,
    pub is_actv: bool,
    pub init_ev: bool,
}

impl Event {
    pub fn empty() -> Event {
        Event {
            tm_stmp: 0.0,
            sh_gate: 0,
            ln_gate: 0,
            ev_type: 0,
            pmt_num: 0,
            is_actv: false,
            init_ev: true,
        }
    }

    pub fn init() -> Event {
        Event {
            tm_stmp: -1.0,
            sh_gate: 0,
            ln_gate: 0,
            ev_type: 0,
            pmt_num: 0,
            is_actv: true,
            init_ev: true,
        }
    }

    pub fn meets_constraints(self, settings: &Config) -> bool {
        let index = InputOutput::pmt_to_index(self.pmt_num);
        if self.ln_gate >= settings.energy_ranges[index][0]
            && self.ln_gate <= settings.energy_ranges[index][1]
            && !(settings.ignore_third_channel && self.pmt_num == settings.third_channel as u32)
            && !(self.ev_type != GAMMA && settings.gamma_only)
        {
            return true;
        }
        false
    }

    pub fn adjust_gain(&mut self, gains: &Vec<f64>) {
        let index = InputOutput::pmt_to_index(self.pmt_num);
        self.ln_gate = (self.ln_gate as f64 * gains[index]) as u32;
    }
}

impl Copy for Event {}

impl Clone for Event {
    fn clone(&self) -> Event {
        *self
    }
}

struct CoincidenceWindow {
    end_time: f64,
    is_opend: bool,
    was_open: bool,
}

pub struct DeadTime {
    str_time: f64,
    end_time: f64,
    is_opend: bool,
    pub total_dt: f64,
}

pub trait State {
    fn get_final_state(&self) -> &FinalState;
}

#[derive(Copy, Clone)]
pub struct FinalState {
    pub total_dt: f64,
    pub total_coinc: [i32; 19],
    pub total_time: f64,
}

impl State for FinalState {
    fn get_final_state(&self) -> &FinalState {
        &self
    }
}

impl<'a> FinalState {
    pub fn new() -> FinalState {
        FinalState {
            total_dt: 0.0,
            total_coinc: [0; 19],
            total_time: 0.0,
        }
    }

    pub fn increment(&mut self, total_dt: f64, total_coinc: [i32; 19], total_time: f64) {
        self.total_dt += total_dt;
        for i in 0..total_coinc.len() {
            self.total_coinc[i] += total_coinc[i];
        }
        self.total_time += total_time;
    }

    pub fn print_footer(&self, settings: &Config) {
        let mut out_foot = String::new();
        let live_time = (self.total_time - self.total_dt) / 1e9;
        let coincidences = if settings.hist_doubles {
            self.total_coinc[D] as f64
        } else if settings.hist_trips {
            self.total_coinc[ABC] as f64
        } else if settings.gamma_only {
            self.total_coinc[G] as f64
        } else {
            self.total_coinc[settings.reference_coinc] as f64
        };
        out_foot += format!("# =========================================\n").as_str();
        out_foot += format!("# Results of the run of the cdt_logic program\n").as_str();
        out_foot += format!("# Cnt  rate: {:10.3}\n", coincidences / live_time).as_str();
        out_foot += format!("# Tot coinc: {:10}\n", coincidences as u64).as_str();
        out_foot += format!("# Live time: {:10.3}\n", live_time).as_str();
        out_foot += format!("# Dead time: {:10.3}\n", self.total_dt / 1e9).as_str();
        out_foot += format!("# Totl time: {:10.3}\n", self.total_time / 1e9).as_str();
        out_foot += format!("# =========================================").as_str();
        println!("{}", out_foot);
    }
}

pub struct CurrentState<'a> {
    pub dead_time: DeadTime,
    coinc_window: CoincidenceWindow,
    coinc_ts: [f64; 19],
    pmt_energy: [u32; 19],
    coinc: [bool; 19],
    pub total_coinc: [i32; 19],
    cycles_passed: i32,
    prev_ts: f64,
    events: [Event; BUFFER_CAPACITY],
    pub current_evt: Event,
    pub last_gamma_evt: Event,
    pub settings: &'a Config,
    reader: &'a mut Readers,
    pub histograms: &'a mut Histograms,
    pub last_state: FinalState,
}

impl<'a> State for CurrentState<'a> {
    fn get_final_state(&self) -> &FinalState {
        &self.last_state
    }
}

impl<'a> CurrentState<'a> {
    pub fn new(
        reader: &'a mut Readers,
        settings: &'a Config,
        histograms: &'a mut Histograms,
    ) -> CurrentState<'a> {
        // If we have sorted timestamps we use only the 0th array item
        // else we put each PMT in its own array item
        let mut events: [Event; BUFFER_CAPACITY] = [Event::init(); BUFFER_CAPACITY];
        for index in 0..4 {
            if !settings.sorted_timestamps {
                let pmt = InputOutput::index_to_pmt(index);
                if let Some(evt) = reader.get_event(pmt) {
                    events[index] = evt;
                }
            } else {
                if let Some(evt) = reader.get_event(0) {
                    events[0] = evt;
                }
                break;
            }
        }
        let last_state = FinalState::new();
        CurrentState {
            dead_time: DeadTime {
                str_time: 0.0,
                end_time: 0.0,
                is_opend: false,
                total_dt: 0.0,
            },
            coinc_window: CoincidenceWindow {
                end_time: 0.0,
                is_opend: false,
                was_open: false,
            },
            settings,
            coinc: [false; 19],
            coinc_ts: [0.0; 19],
            pmt_energy: [0; 19],
            total_coinc: [0; 19],
            cycles_passed: 0,
            prev_ts: 0.,
            events,
            current_evt: Event::empty(),
            last_gamma_evt: Event::empty(),
            reader,
            histograms,
            last_state,
        }
    }

    fn add_coincidences(&mut self) {
        self.coinc[AB] = self.coinc[A] && self.coinc[B];
        self.coinc[BC] = self.coinc[B] && self.coinc[C];
        self.coinc[AC] = self.coinc[A] && self.coinc[C];
        self.coinc[ABC] = self.coinc[A] && self.coinc[B] && self.coinc[C];
        self.coinc[D] = self.coinc[AB] || self.coinc[BC] || self.coinc[AC];
        self.coinc[S] = self.coinc[A] || self.coinc[B] || self.coinc[C];

        self.coinc[AG] = self.coinc[A] && self.coinc[G];
        self.coinc[BG] = self.coinc[B] && self.coinc[G];
        self.coinc[CG] = self.coinc[C] && self.coinc[G];
        self.coinc[ABG] = self.coinc[AB] && self.coinc[G];
        self.coinc[BCG] = self.coinc[BC] && self.coinc[G];
        self.coinc[ACG] = self.coinc[AC] && self.coinc[G];
        self.coinc[TG] = self.coinc[ABC] && self.coinc[G];
        self.coinc[DG] = self.coinc[D] && self.coinc[G];
        self.coinc[SG] = self.coinc[S] && self.coinc[G];

        // Histogram
        if self.settings.show_time_hist {
            self.update_time_hist();
        }

        if self.settings.show_energy_hist && self.coinc[G] {
            self.increment_energy_hist();
        }

        if self.settings.show_pmt_energy {
            self.increment_pmt_energy_hist();
        }

        for i in 0..self.coinc.len() {
            if self.coinc[i] {
                self.total_coinc[i] += 1
            };
            self.coinc[i] = false;
            self.pmt_energy[i] = 0;
        }
    }

    fn update_time_hist(&mut self) {
        // Shorthand for the reference_channel and the secondary_channel
        let rc = self.settings.reference_channel;
        let sc = self.settings.secondary_channel;

        // The reference coincidence is defined as rc + sc in the case of
        // double coincidences, D in the case of the logical sum of doubles or
        // T in the case of triple coincidences.
        let ref_coinc = self.settings.reference_coinc;

        //If we have a reference coincidence in channels AB, BC or AC
        if self.coinc[ref_coinc] && !(self.settings.hist_trips || self.settings.hist_doubles) {
            // Tripple veto: do not increment the histogram if we have a triple coincidence
            if self.settings.triple_veto && !self.coinc[ABC] {
                self.increment_time_hist(self.coinc_ts[rc], self.coinc_ts[sc]);
            } else if !self.settings.triple_veto {
                // Get the timestamps of the first arriving between the reference and
                // secondary channels; Get the timestamp of the third channel i.e. the
                // channel that is not the reference and not the secondary.
                let third = match self.settings.gamma_veto {
                    false => self.coinc_ts[ABC - rc - sc],
                    true => 0.0,
                };
                let first = f64::min(self.coinc_ts[rc], self.coinc_ts[sc]);

                // This is due to the fact that all double coincidences are also
                // triple coincidences. Thus, if we observe the third channel before the
                // first arriving in one of the channels that we are interested in, we must
                // count the time difference of a triple coincidence.
                if third >= first || third == 0.0 {
                    self.increment_time_hist(self.coinc_ts[rc], self.coinc_ts[sc]);
                } else {
                    let (ref_ts, abc_ts) = self.get_ordered(0, 2);
                    self.increment_time_hist(ref_ts, abc_ts);
                }
            }
        // Count triples only
        } else if self.settings.hist_trips && self.coinc[ABC] {
            let (ref_ts, abc_ts) = self.get_ordered(0, 2);
            self.increment_time_hist(ref_ts, abc_ts);
        // Count logical sum of doubles
        } else if self.coinc[D] && self.settings.hist_doubles {
            let (ref_ts, d_ts) = self.get_ordered(0, 1);
            self.increment_time_hist(ref_ts, d_ts);
        }
        self.clear_hist_events();
    }

    fn increment_time_hist(&mut self, reference_ts: f64, secondary_ts: f64) {
        let time_delta = secondary_ts - reference_ts;
        let mid = self.settings.mid as f64;
        let td: f64;
        if !self.settings.asymmetrical_doubles {
            td = (time_delta / self.settings.bin_size + mid).round();
        } else {
            td = (time_delta.abs() / self.settings.bin_size + mid).round();
        }
        // println!("{} {}", self.last_gamma_evt.ln_gate, time_delta);
        if self.settings.timing_plot {
            let x_nbins = self.histograms.timing.len();
            let y_nbins = self.histograms.timing[0].len();
            let ln_gate_bin = (self.last_gamma_evt.ln_gate as f64
                / self.settings.n_energy_bins as f64
                * x_nbins as f64) as usize;
            let td_bin = ((time_delta + mid) / (mid * 2.0) * y_nbins as f64) as usize;
            if ln_gate_bin < x_nbins && td_bin < y_nbins {
                self.histograms.timing[ln_gate_bin][td_bin] += 1;
            }
        }
        match self.histograms.time_delta.increment(td as u64) {
            Ok(_) => (),
            Err(_) => (),
        };
    }

    fn increment_pmt_energy_hist(&mut self) {
        self.pmt_energy[AB] = if self.coinc[AB] {
            self.pmt_energy[A] + self.pmt_energy[B]
        } else {
            0
        };
        self.pmt_energy[BC] = if self.coinc[BC] {
            self.pmt_energy[B] + self.pmt_energy[C]
        } else {
            0
        };
        self.pmt_energy[AC] = if self.coinc[AC] {
            self.pmt_energy[A] + self.pmt_energy[C]
        } else {
            0
        };
        self.pmt_energy[D] = match self.coinc[D] {
            true => self.pmt_energy[A] + self.pmt_energy[B] + self.pmt_energy[C],
            false => 0,
        };
        self.pmt_energy[ABC] = match self.coinc[ABC] {
            true => self.pmt_energy[A] + self.pmt_energy[B] + self.pmt_energy[C],
            false => 0,
        };

        for (i, h) in &mut self.histograms.pmt_energy.iter_mut().enumerate() {
            let energy =
                (self.pmt_energy[i] as f64 / self.settings.energy_bin_size as f64).floor() as u64;
            if energy > 0 {
                match h.increment(energy) {
                    Ok(_) => (),
                    Err(_) => (),
                }
            }
        }
    }

    fn increment_energy_hist(&mut self) {
        let mut gate = self.last_gamma_evt.ln_gate;
        if gate > self.settings.energy_ranges[3][1] {
            gate = self.settings.energy_ranges[3][1];
        }
        let energy = (gate as f64 / self.settings.energy_bin_size as f64).floor() as u64;
        match self.histograms.energy.increment(energy) {
            Ok(_) => (),
            Err(_) => (),
        };
        self.histograms.tdcr_vs_energy[energy as usize].increment(energy, self.coinc);
    }

    fn get_ordered(&mut self, ref_chn: usize, sec_chn: usize) -> (f64, f64) {
        let timestamps = [self.coinc_ts[A], self.coinc_ts[B], self.coinc_ts[C]];
        let mut order: Vec<f64> = timestamps
            .iter()
            .filter_map(|x| if *x == 0.0 { None } else { Some(*x) })
            .collect();
        order.sort_by(|x, y| CurrentState::cmp_f64(x, y));
        return (*order.get(ref_chn).unwrap(), *order.get(sec_chn).unwrap());
    }

    fn cmp_f64(a: &f64, b: &f64) -> Ordering {
        if a < b {
            return Ordering::Less;
        } else if a > b {
            return Ordering::Greater;
        }
        return Ordering::Equal;
    }

    fn clear_hist_events(&mut self) {
        for i in 0..self.coinc_ts.len() {
            self.coinc_ts[i] = 0.0;
        }
    }

    fn is_cw_open(&self) -> bool {
        self.coinc_window.is_opend
    }

    fn was_cw_open(&self) -> bool {
        self.coinc_window.was_open
    }

    fn is_dt_open(&self) -> bool {
        self.dead_time.is_opend
    }

    fn get_cw_end(&self) -> f64 {
        self.coinc_window.end_time
    }

    fn get_dt_end(&self) -> f64 {
        self.dead_time.end_time
    }

    fn open_cw(&mut self) {
        self.coinc_window.is_opend = true;
        self.set_cw_end(self.current_evt.tm_stmp + self.settings.cw_duration);
        self.events[CW_END as usize] = Event {
            tm_stmp: self.get_cw_end(),
            ev_type: CW_END,
            ..self.current_evt
        };
        self.open_dt();
    }

    fn open_dt(&mut self) {
        self.dead_time.str_time = self.current_evt.tm_stmp;
        self.set_dt_end(self.current_evt.tm_stmp + self.settings.ext_dt);
    }

    fn close_cw(&mut self) {
        self.coinc_window.is_opend = false;
        self.events[DT_END as usize] = Event {
            tm_stmp: self.get_dt_end(),
            ev_type: DT_END,
            ..self.current_evt
        };
    }

    fn close_dt(&mut self) {
        let dt_extension = self.current_evt.tm_stmp - self.dead_time.str_time;
        self.dead_time.total_dt += dt_extension;
    }

    fn calculate_state(&mut self) {
        self.coinc_window.was_open = self.coinc_window.is_opend;
        self.dead_time.is_opend =
            self.current_evt.tm_stmp < self.dead_time.end_time && !self.coinc_window.is_opend;
    }

    fn set_dt_end(&mut self, new_dt: f64) {
        self.dead_time.end_time = new_dt;
    }

    fn set_cw_end(&mut self, new_cw: f64) {
        self.coinc_window.end_time = new_cw;
    }

    fn check_new_ts(&mut self) -> bool {
        // DEBUG
        if self.current_evt.tm_stmp < self.prev_ts - 1.0 {
            println!(
                "\rCurrent timestamp is less than previous ts! {} {} {}",
                self.current_evt.tm_stmp, self.current_evt.pmt_num, self.prev_ts,
            );
            self.current_evt.tm_stmp = self.prev_ts;
            return true;
        };
        if self.current_evt.tm_stmp > self.settings.time_limit * 1e9
            && self.settings.time_limit != 0.0
        {
            return false;
        }
        self.prev_ts = self.current_evt.tm_stmp;
        true
    }

    fn _calculate_buffer_size(&mut self) -> u32 {
        self.cycles_passed += 1;
        let mut buffered_signals: u32 = 0;
        for i in 0..self.events.len() {
            if self.events[i].ev_type < CW_END {
                buffered_signals += 1;
            }
        }
        buffered_signals
    }

    fn get_next_event(&mut self) -> Option<Event> {
        self.cycles_passed += 1;
        let evt = match self
            .events
            .iter()
            .filter(|ev| !ev.init_ev)
            .min_by_key(|ev| ev.tm_stmp as i64)
        {
            Some(e) => *e,
            None => panic!("Cannot fetch next event! Prev ts: {}", self.prev_ts),
        };

        if evt.ev_type == CW_END || evt.ev_type == DT_END {
            self.events[evt.ev_type as usize] = Event::init();
            return Some(evt);
        }

        let pmt_sampled = evt.pmt_num;
        loop {
            if let Some(mut decay) = self.reader.get_event(pmt_sampled) {
                if decay.is_actv == false {
                    return None;
                };
                decay.adjust_gain(&self.settings.gain_adjustment);
                // TODO fix overflow over 2^48 ns
                let index = match self.settings.sorted_timestamps {
                    false => InputOutput::pmt_to_index(decay.pmt_num),
                    true => 0,
                };
                let prev_ts = self.events[index].tm_stmp;
                if decay.tm_stmp < prev_ts && self.current_evt.tm_stmp <= 2.0_f64.powf(47.999) {
                    // HACK
                    continue;
                }
                if decay.tm_stmp < prev_ts && prev_ts > 2.0_f64.powf(47.9) {
                    decay.tm_stmp += 2.0_f64.powf(48.0);
                }
                self.events[index] = decay;
                break;
            } else {
                return None;
            }
        }
        Some(evt)
    }

    fn extend_dead_time(&mut self) {
        self.set_dt_end(self.current_evt.tm_stmp + self.settings.ext_dt);
        self.events[DT_END as usize] = Event {
            tm_stmp: self.get_dt_end(),
            ev_type: DT_END,
            ..self.current_evt
        };
    }

    fn _process_gamma_event(&mut self) {
        let curr_evt_id = self.current_evt.pmt_num as usize;
        if self.settings.gamma_veto
            && (self.is_cw_open() || self.settings.gamma_only)
            && !self.is_dt_open()
        {
            self.coinc[curr_evt_id] = true;
            if self.settings.show_time_hist || self.settings.show_energy_hist {
                self.coinc_ts[curr_evt_id] = self.current_evt.tm_stmp;
                self.last_gamma_evt = self.current_evt;
            }
        }
        if self.settings.gamma_only && !self.is_dt_open() {
            self.open_dt();
            self.add_coincidences();
        } else if self.settings.gamma_only {
            self.extend_dead_time();
        }
    }

    pub fn analyze_events(&mut self) -> &CurrentState {
        loop {
            let current_evt = match self.get_next_event() {
                Some(e) => e,
                None => break,
            };
            self.current_evt = current_evt;
            let curr_evt_id = self.current_evt.pmt_num as usize;
            let meets_constraints = current_evt.meets_constraints(&self.settings);

            // Calculate the new state of the system
            self.calculate_state();

            //PMT event
            if self.current_evt.ev_type == PHOTON || self.current_evt.ev_type == GAMMA {
                if self.is_dt_open() {
                    // Extend the dead time
                    if !self.settings.non_extended_deadtime {
                        self.extend_dead_time();
                    };
                } else if !self.was_cw_open() && !self.is_dt_open() {
                    if !meets_constraints {
                        self.close_cw();
                        self.open_dt();
                        continue;
                    };
                    self.open_cw();
                    self.coinc[curr_evt_id] = true;
                    let is_first_in_pmt = self.coinc_ts[curr_evt_id] == 0.0;
                    if self.settings.show_time_hist && is_first_in_pmt {
                        // Add ts as reference channel in histogram
                        self.coinc_ts[curr_evt_id] = self.current_evt.tm_stmp;
                    }
                    if self.settings.show_pmt_energy && is_first_in_pmt {
                        self.pmt_energy[curr_evt_id] = self.current_evt.ln_gate;
                    }
                    if self.current_evt.ev_type == GAMMA && is_first_in_pmt {
                        self.last_gamma_evt = self.current_evt;
                    }
                } else if self.was_cw_open() && self.is_cw_open() {
                    if !meets_constraints {
                        self.close_cw();
                        self.open_dt();
                        continue;
                    };
                    let curr_evt_id = self.current_evt.pmt_num as usize;
                    self.coinc[curr_evt_id] = true;
                    // Add ts for histogram calcualtion
                    let is_first_in_pmt = self.coinc_ts[curr_evt_id] == 0.0;
                    if self.settings.show_time_hist && is_first_in_pmt {
                        self.coinc_ts[curr_evt_id] = self.current_evt.tm_stmp;
                    }
                    if self.settings.show_pmt_energy && is_first_in_pmt {
                        self.pmt_energy[curr_evt_id] = self.current_evt.ln_gate;
                    }
                    if self.current_evt.ev_type == GAMMA && is_first_in_pmt {
                        self.last_gamma_evt = self.current_evt;
                    }
                }
            // Closing of dead time window
            } else if self.current_evt.tm_stmp == self.get_dt_end()
                && self.current_evt.ev_type == DT_END
            {
                self.close_dt();
            // Closing of coincidence window
            } else if self.current_evt.ev_type == CW_END {
                self.close_cw();
                self.add_coincidences();
            }

            if !self.check_new_ts() {
                break;
            }
            if self.cycles_passed % 1_000_000 == 0 {
                if !self.settings.hide_tdcr && !self.settings.disable_live_cps {
                    self.update_last_state();
                    print!("\r");
                    InputOutput::print_counting_rates(self, &self.settings);
                }
            }
        }
        if !self.settings.hide_tdcr {
            self.update_last_state();
            if !self.settings.disable_live_cps {
                print!("\r");
            }
            InputOutput::print_counting_rates(self, &self.settings);
            println!();
        }
        self
    }

    fn update_last_state(&mut self) {
        self.last_state.total_dt = self.dead_time.total_dt.clone();
        self.last_state.total_coinc = self.total_coinc.clone();
        self.last_state.total_time = self.current_evt.tm_stmp.clone();
    }
}
